/*
** EPITECH PROJECT, 2018
** include/pshswp.h
** File description:
** header file for pushswap
*/

#ifndef PSHSWP_H_
#define PSHSWP_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define EXIT    84

typedef struct Node {
    int nb;
    struct Node *next;
}Node;

typedef struct Numbers {
    Node *l_a;
    Node *l_b;
}Numbers;

int my_getnbr(char const *str);
void my_putchar(char c);
int my_putstr(char const *str);
int my_put_nbr(int nb);
void make_lists(int ac, char **av, Node **l_a, Node **l_b);
void print_list(Node *l);
int pa(Node **l_a, Node **l_b);
int pb(Node **l_a, Node **l_b);
void ra(Node **l_a);
void rb(Node **l_b);
void rr(Node **l_a, Node **l_b);
void rra(Node **l_a);
void rrb(Node **l_a);
void rrr(Node **l_a, Node **l_b);
void sa(Node **l_a);
void sb(Node **l_b);
void sc(Node **l_a, Node **l_b);
int append(Node** head_ref, int new_nb);
int size_nbr(int nb);
int test_list(Node **l_a);
void radix_sort(Node **l_a, Node **l_b, int ac);
int sort_list(Node **l_a, Node **l_b, int ac);

#endif
