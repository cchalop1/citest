/*
** EPITECH PROJECT, 2018
** main.c
** File description:
** main for push swap
*/

#include "pshswp.h"

int main(int ac, char **av)
{
    Node *l_a = NULL;
    Node *l_b = NULL;

    if (ac < 3) {
        my_putchar('\n');
        return EXIT;
    }
    ac -= 1;

    make_lists(ac, av, &l_a, &l_b);
    if (test_list(&l_a) == 84) {
        my_putchar('\n');
        return EXIT;
    }
    sort_list(&l_a, &l_b, ac);
    // radix_sort(&l_a, &l_b, ac);
    print_list(l_a);
    // printf("\n");
    // print_list(l_b);
    return 0;
}
