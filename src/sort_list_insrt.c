/*
** EPITECH PROJECT, 2018
** src/sort_list_insrt.c
** File description:
** file for insert sort
*/

#include "pshswp.h"

// int get_min_idx_node(Node *l_a)
// {
//     Node *current = l_a;
//     int nb_min = current->nb;
//     int idx = 0;
//
//     current = current->next;
//     while (current != NULL) {
//         if (current->nb < nb_min) {
//             nb_min = current->nb;
//         }
//         current = current->next;
//     }
//     current = l_a;
//     while (current->nb != nb_min) {
//         idx += 1;
//         current = current->next;
//     }
//     return (idx + 1);
// }
//
// void radix_sort(Node **l_a, Node **l_b, int ac)
// {
//     int count = 0;
//     int idx;
//     int i = 0;
//     Node *l_a_node = *l_a;
//
//     while (i < (ac - 1)) {
//         idx = get_min_idx_node(l_a_node);
//         while (count < (idx - 1)) {
//             ra(l_a);
//             my_putstr("ra ");
//             l_a_node = *l_a;
//             count += 1;
//         }
//         count = 0;
//         pa(l_a, l_b);
//         my_putstr("pa ");
//         l_a_node = *l_a;
//         i += 1;
//     }
//     i = 0;
//     while (i < (ac - 1)) {
//         pb(l_a, l_b);
//         if (i == (ac - 2))
//             my_putstr("pa");
//         else
//             my_putstr("pa ");
//         i += 1;
//     }
// }

int list_is_sort(Node **l_a)
{
    Node *current = *l_a;

    while (current->next != NULL) {
        if (current->nb > current->next->nb) {
            return 0;
        }
        current = current->next;
    }
    return 1;
}


int sort_list(Node **l_a, Node **l_b, int ac)
{
    Node *current_a = *l_a;
    int i = 0;
    int idx = 0;
    int size = 2147483646;
    char *buf = malloc(size);

    while (list_is_sort(l_a) != 1) {
        if (idx > 2147480000) {
            write(1, buf, idx - 1);
            idx = 0;
        }
        while (current_a->nb <= current_a->next->nb) {
            i += 1;
            pa(l_a, l_b);
            current_a = *l_a;

            buf[idx] = 'p';
            buf[idx + 1] = 'a';
            buf[idx + 2] = ' ';
            idx += 3;

            if (current_a->next == NULL) {
                return 0;
            }
        }
        sa(l_a);
        current_a = *l_a;
        buf[idx] = 's';
        buf[idx + 1] = 'a';
        buf[idx + 2] = ' ';
        idx += 3;
        while (i > 0) {
            pb(l_a, l_b);
            current_a = *l_a;
            buf[idx] = 'p';
            buf[idx + 1] = 'b';
            buf[idx + 2] = ' ';
            idx += 3;
            i -= 1;
        }
    }
    write(1, buf, idx - 1);
    return 1;
}
