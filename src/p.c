/*
** EPITECH PROJECT, 2018
** src/p.c
** File description:
** function for push first element in other list
*/

#include "pshswp.h"

int pa(Node **l_a, Node **l_b)
{
    int tmp;
    Node *ref_a = *l_a;

    tmp = ref_a->nb;
    Node *new_node_b = malloc(sizeof(Node));
    new_node_b->nb  = tmp;
    new_node_b->next = (*l_b);
    (*l_b) = new_node_b;

    Node *delet_node = NULL;

    if (*l_a == NULL) {
        return 0;
    }

    delet_node = (*l_a)->next;
    free(*l_a);
    *l_a = delet_node;
    return 1;
}

int pb(Node **l_a, Node **l_b)
{
    int tmp;

    Node *ref_b = *l_b;
    tmp = ref_b->nb;
    Node *new_node_a = malloc(sizeof(Node));
    new_node_a->nb  = tmp;
    new_node_a->next = (*l_a);
    (*l_a) = new_node_a;

    Node *delet_node = NULL;

    if (*l_b == NULL) {
        return 0;
    }

    delet_node = (*l_b)->next;
    free(*l_b);
    *l_b = delet_node;
    return 1;
}
