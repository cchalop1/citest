/*
** EPITECH PROJECT, 2018
** src/test_list.c
** File description:
** function for test list is sorted
*/

#include "pshswp.h"

int test_list(Node **l_a)
{
    Node *current = *l_a;

    while (current->next != NULL) {
        if (current->nb > current->next->nb) {
            return 1;
        }
        current = current->next;
    }
    return 84;
}
