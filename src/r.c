/*
** EPITECH PROJECT, 2018
** src/r.c
** File description:
** function for rotation list
*/

#include "pshswp.h"

void ra(Node **l_a)
{
    Node *current = *l_a;
    Node *first_node = current;

    while (current->next != NULL)
        current = current->next;
    current->next = *l_a;
    *l_a = first_node->next;
    first_node->next = NULL;
}

void rb(Node **l_b)
{
    Node *current = *l_b;
    Node *first_node = current;

    while (current->next != NULL)
        current = current->next;
    current->next = *l_b;
    *l_b = first_node->next;
    first_node->next = NULL;
}

void rr(Node **l_a, Node **l_b)
{
    ra(l_a);
    rb(l_b);
}
