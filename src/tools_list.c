/*
** EPITECH PROJECT, 2018
** src/tools_list.c
** File description:
** tools for lists
*/

#include "pshswp.h"

int append(Node** head_ref, int new_nb)
{
    Node* new_node = malloc(sizeof(Node));
    Node *last = *head_ref;
    new_node->nb  = new_nb;
    new_node->next = NULL;
    if (*head_ref == NULL) {
       *head_ref = new_node;
       return 0;
    }
    while (last->next != NULL)
        last = last->next;
    last->next = new_node;
    return 1;
}

void print_list(Node *l)
{
    while (l != NULL) {
        my_putchar(' ');
        my_put_nbr(l->nb);
        my_putchar(' ');
        l = l->next;
    }
}

void make_lists(int ac, char **av, Node **l_a, Node **l_b)
{
    for (int i = 1; i <= ac; i += 1) {
        append(l_a, my_getnbr(av[i]));
    }
}
