/*
** EPITECH PROJECT, 2018
** src/rr.c
** File description:
** fonction for rotate liste to right
*/

#include "pshswp.h"

void rra(Node **l_a)
{
    int tmp;
    Node *current = *l_a;
    Node *first_node = malloc(sizeof(Node));

    while (current->next != NULL)
        current = current->next;
    tmp = current->nb;
    first_node->nb = tmp;
    first_node->next = *l_a;
    *l_a = first_node;

    current = *l_a;
    while (current->next->next != NULL)
        current = current->next;
    current->next = NULL;
}

void rrb(Node **l_b)
{
    int tmp;
    Node *current = *l_b;
    Node *first_node = malloc(sizeof(Node));

    while (current->next != NULL)
        current = current->next;
    tmp = current->nb;
    first_node->nb = tmp;
    first_node->next = *l_b;
    *l_b = first_node;

    current = *l_b;
    while (current->next->next != NULL)
        current = current->next;
    current->next = NULL;
}

void rrr(Node **l_a, Node **l_b)
{
    rra(l_a);
    rrb(l_b);
}
