/*
** EPITECH PROJECT, 2018
** getnbr
** File description:
** getnbr
*/

int my_getnbr(char const *str)
{
    int i = 0;
    int result = 0;
    int neg = 1;

    if (str[0] == '-') {
        neg = neg * -1;
        i++;
    }
    while (str[i] != '\0') {
        if (str[i] >= '0' && str[i] <= '9') {
            result = result + (str[i] - 48);
            result = result * 10;
        } else
            break;
        i++;
    }
    result = result / 10;
    result = result * neg;
    return (result);
}
