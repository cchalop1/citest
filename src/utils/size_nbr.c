/*
** EPITECH PROJECT, 2018
** size_nbr.c
** File description:
** return nbr dig the nbr
*/

int size_nbr(int nb)
{
    int result = 0;

    while (nb != 0) {
        nb /= 10;
        result += 1;
    }
    return result;
}
