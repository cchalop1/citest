/*
** EPITECH PROJECT, 2018
** MY_PUT_NBR
** File description:
** dislay char
*/

#include <stdio.h>

void my_putchar(char c);

int neg(int nb)
{
    if (nb < 0) {
        my_putchar('-');
        return (-nb);
    } else
        return nb;
}

int start_nb(int nb)
{
    int p = 1;
    int result_nb = nb;

    if (nb <= 9) {
        my_putchar(result_nb + 48);
        return 0;
    }
    while (result_nb > 9) {
        result_nb = result_nb / 10;
        p = p * 10;
    }
    my_putchar(result_nb + 48);
    if (nb % p == 0)
        p = p / 10;
    while (nb % p < p / 10) {
        my_putchar('0');
        p = p / 10;
    }

    return (start_nb(nb % p));
}

int my_put_nbr(int nb)
{
    start_nb(neg(nb));
    return 0;
}
