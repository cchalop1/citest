/*
** EPITECH PROJECT, 2018
** src/s.c
** File description:
** function for switch tow firs arg
*/

#include "pshswp.h"

void sa(Node **l_a)
{
    int tmp;

    Node *new_node = *l_a;
    tmp = new_node->nb;
    new_node->nb = new_node->next->nb;
    new_node->next->nb = tmp;
}

void sb(Node **l_b)
{
    int tmp;

    Node *new_node = *l_b;
    tmp = new_node->nb;
    new_node->nb = new_node->next->nb;
    new_node->next->nb = tmp;
}

void sc(Node **l_a, Node **l_b)
{
    sa(l_a);
    sa(l_b);
}
