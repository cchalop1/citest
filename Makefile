##
## EPITECH PROJECT, 2018
## main.c
## File description:
## main for push swap
##

SRC  	=		./src/*.c

UTILS  	=		./src/utils/*.c

NAME	=		push_swap

NAMETEST	=		unit_test

MAIN	=		main.c

HEAD	=		-I./include/

FLAGS	=		-Wall -Wextra -Werror

FILESTEST	=	tests/*.c

TFLAGS	=		--coverage -lcriterion

all:
	gcc -o $(NAME) $(MAIN) $(SRC) $(UTILS) $(HEAD)

tests_run: re
	gcc -o $(NAMETEST) $(FILESTEST) $(SRC) $(UTILS) $(HEAD) $(TFLAGS)
	./unit_test

clean:
	rm -f *~
	rm -f *.swp
	rm -f *#
	rm -f unit_test
	rm -f a.out
	rm -f *.gcno
	rm -f *.gcda
	rm -f vgcore.*

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re tests_run
