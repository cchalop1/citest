/*
** EPITECH PROJECT, 2018
** tests/test_swap.c
** File description:
** some test for for function s
*/

#include <criterion/criterion.h>
#include "pshswp.h"

Test(sa, test_sa)
{
    Node* l_a = NULL;
    Node* l_b = NULL;
    int ac = 5;
    char *av[] = {" ", "22", "44", "66", "88"};

    make_lists((ac - 1), av, &l_a, &l_b);
    sa(&l_a);
    cr_assert_eq(l_a->nb, 44);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 22);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 66);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 88);
}

Test(pa, test_pa)
{
    Node* l_a = NULL;
    Node* l_b = NULL;
    int ac = 5;
    char *av[] = {" ", "22", "44", "66", "88"};

    make_lists((ac - 1), av, &l_a, &l_b);
    pa(&l_a, &l_b);
    cr_assert_eq(l_a->nb, 44);
    cr_assert_eq(l_b->nb, 22);
}

Test(ra, test_ra)
{
    Node* l_a = NULL;
    Node* l_b = NULL;
    int ac = 5;
    char *av[] = {" ", "22", "44", "66", "88"};

    make_lists((ac - 1), av, &l_a, &l_b);
    ra(&l_a);
    cr_assert_eq(l_a->nb, 44);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 66);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 88);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 22);
}

Test(rra, test_rra)
{
    Node* l_a = NULL;
    Node* l_b = NULL;
    int ac = 5;
    char *av[] = {" ", "22", "44", "66", "88"};

    make_lists((ac - 1), av, &l_a, &l_b);
    rra(&l_a);
    cr_assert_eq(l_a->nb, 88);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 22);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 44);
    l_a = l_a->next;
    cr_assert_eq(l_a->nb, 66);
}
