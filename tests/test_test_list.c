/*
** EPITECH PROJECT, 2018
** tests/test_test_list.c
** File description:
** test function for test list
*/

#include <criterion/criterion.h>
#include "pshswp.h"

Test(test_list, test_test_list)
{
    Node* l_a = NULL;
    Node* l_b = NULL;
    int ac = 5;
    char *av[] = {" ", "5", "2", "8", "4"};

    make_lists((ac - 1), av, &l_a, &l_b);
    cr_assert_eq(test_list(&l_a), 1);
}

Test(test_list, test_test_list_sorted)
{
    Node* l_a = NULL;
    Node* l_b = NULL;
    int ac = 5;
    char *av[] = {" ", "1", "2", "3", "4"};

    make_lists((ac - 1), av, &l_a, &l_b);
    cr_assert_eq(test_list(&l_a), 84);
}
